import java.util.Scanner;

public class PartThree{
	public static void main(String[]args){
		Scanner scanner = new Scanner(System.in);
		int firstNumber = scanner.nextInt();
		int secondNumber = scanner.nextInt();
		int sum = Calculator.add(firstNumber, secondNumber);
		System.out.println(sum);
		int subtract = Calculator.subtract(firstNumber, secondNumber);
		System.out.println(subtract);
		
		Calculator calculator = new Calculator();
		int multiply = calculator.multiply(firstNumber, secondNumber);
		System.out.println(multiply);
		
		int division = calculator.division(firstNumber, secondNumber);
		System.out.println(division);
	}
}